/*
Nate Hopkins, 6-18-16

Many of the function calls take parameters direction and pwm.
direction is a value in the MotorState enum (0-3) and pwm should be an unsigned integer value between 0 and 255
*/

enum MotorState {
	brakeToVcc,
	retract,
	extend,
	brakeToGnd
};

// Digital pins:
const uint8_t inputApin = 4; // the combination of these two pins gives all motor states in MotorState
const uint8_t inputBpin = 7;
const uint8_t pwmPin = 5;
const uint8_t overCurrentPin = 13; // used to indicate an overcurrent condition

// Analog pins:
const uint8_t currentSensePin = 0;
const uint8_t positionFeedbackPin = 2; // connected to the internal pot in the actuator
const uint8_t potPin = 3; // just for debugging
const uint8_t photoCellEastPin = 4; // connect VCC to one side of the photocell.  Connect the other side to these pins and to a 1.5k resistor.
const uint8_t photoCellWestPin = 5; // connect the other side of the resistor to ground.

// Constants:
const uint16_t maxExtend = 900;
const uint16_t minExtend = 100;
const uint16_t shadowThreshold = 500;
const uint16_t nightThreshold = 100;
const uint16_t currentSenseThreshold = 100;
const uint16_t motorDriveIncrement = 1000; // in milliseconds
const uint16_t rampPeriod = 250;
const uint16_t rampSteps = 100;

// Global variables:
bool wasLastTimeRetract = false;
bool wasLastTimeExtend = false;

void setup() {
	Serial.begin(9600);

	pinMode(inputApin, OUTPUT);
	pinMode(inputBpin, OUTPUT);
	pinMode(pwmPin, OUTPUT);
	pinMode(potPin, INPUT);
	pinMode(photoCellEastPin, INPUT);
	pinMode(photoCellWestPin, INPUT);
	pinMode(currentSensePin, INPUT);
	pinMode(overCurrentPin, OUTPUT);

	// Initialize stopped
	stopMotor();

	// while(millis() < 8000) { startMotor(extend, 255); }
	// startMotor(retract, 255);
	// delay(4000);
}

void loop() {
	uint16_t feedback = analogRead(positionFeedbackPin);
	uint8_t speed = map(analogRead(potPin), 0, 1023, 0, 255);
	uint16_t currentSense = analogRead(currentSensePin);
	uint16_t eastLight = analogRead(photoCellEastPin);
	uint16_t westLight = analogRead(photoCellWestPin);

	if((eastLight > shadowThreshold) && (westLight < shadowThreshold) && (feedback > minExtend)) { // too far east, move west
		Serial.print("retract");
		Serial.print("\t");
		if(wasLastTimeRetract == false) {
			if(wasLastTimeExtend == true) {
				softStopMotor(speed);
			}
			advanceMotor(retract, speed, motorDriveIncrement, true); // TODO: check direction
		}
		else {
			advanceMotor(retract, speed, motorDriveIncrement, false); // TODO: check direction
		}
		wasLastTimeRetract = true;
	}
	else if((eastLight < shadowThreshold) && (westLight > shadowThreshold) && (feedback < maxExtend)) { // too far west, move east
		Serial.print("extend");
		Serial.print("\t");
		if(wasLastTimeExtend == false) {
			if(wasLastTimeRetract == true) {
				softStopMotor(speed);
			}
			advanceMotor(extend, speed, motorDriveIncrement, true); // TODO: check direction
		}
		else {
			advanceMotor(extend, speed, motorDriveIncrement, false); // TODO: check direction
		}
		wasLastTimeExtend = true;
	}
	else if((eastLight < nightThreshold) && (westLight < nightThreshold) && (feedback >= maxExtend)) { // night
		Serial.print("night");
		Serial.print("\t");
		if(wasLastTimeRetract == true || wasLastTimeExtend == true) {
			softStopMotor(speed);
		}
		returnMotorToEast(1023);
		wasLastTimeRetract = false;
		wasLastTimeExtend = false;
	}
	else {
		softStopMotor(speed);
		wasLastTimeRetract = false;
		wasLastTimeExtend = false;
	}
	// if both are over shadowThreshold, it's looking at the sun

	Serial.print(feedback);
	Serial.print("\t");
	Serial.print(speed);
	Serial.print("\t");
	Serial.print(eastLight);
	Serial.print("\t");
	Serial.print(westLight);
	Serial.println("\t");

	if (analogRead(currentSensePin) < currentSenseThreshold) {
		digitalWrite(overCurrentPin, HIGH);
	}
}

void startMotor(uint8_t direction, uint8_t pwm) {
	if(direction == retract) {
		digitalWrite(inputApin, HIGH);
		digitalWrite(inputBpin, LOW);
	}
	else if(direction == extend) {
		digitalWrite(inputApin, LOW);
		digitalWrite(inputBpin, HIGH);
	}
	
	analogWrite(pwmPin, pwm);
}

void softStartMotor(uint8_t direction, uint8_t pwm) {
	if(direction == retract) {
		digitalWrite(inputApin, HIGH);
		digitalWrite(inputBpin, LOW);
	}
	else if(direction == extend) {
		digitalWrite(inputApin, LOW);
		digitalWrite(inputBpin, HIGH);
	}

	uint32_t startTime = millis();
	float stepDelay = rampPeriod / rampSteps; // 2.5 ms
	float pwmStep = pwm / rampSteps; // for full pwm input, 1023 / 100 = 10.23
	float currentPwm = 0;

	for(int i = 0; i < rampSteps; i++) {
		currentPwm += pwmStep;
		if(pwm > 255) {
			analogWrite(pwmPin, 255);
		}
		else {
			analogWrite(pwmPin, currentPwm);
		}
		delayMicroseconds(stepDelay*1000);
	}
	analogWrite(pwmPin, pwm);
}

void stopMotor() {
	digitalWrite(inputApin, LOW);
	digitalWrite(inputBpin, LOW);
	analogWrite(pwmPin, 0);
}

void softStopMotor(uint8_t pwm) {
	digitalWrite(inputApin, LOW);
	digitalWrite(inputBpin, LOW);

	uint32_t startTime = millis();
	float stepDelay = rampPeriod / rampSteps; // 2.5 ms
	float pwmStep = pwm / rampSteps; // for full pwm input, 1023 / 100 = 10.23

	for(int i = 0; i < rampSteps; i++) {
		pwm -= pwmStep;
		if(pwm < 0) {
			analogWrite(pwmPin, 0);
		}
		else {
			analogWrite(pwmPin, pwm);
		}
		delayMicroseconds(stepDelay*1000);
	}
	analogWrite(pwmPin, 0);
}

void advanceMotor(uint8_t direction, uint8_t pwm, uint16_t duration, bool softStart) {
	uint32_t startTime = millis();

	if(softStart) {
		softStartMotor(direction, pwm);
	}
	else {
		startMotor(direction, pwm);
	}

	while(millis() - startTime < duration) {}
}

void returnMotorToEast(uint8_t pwm) {
	uint16_t feedback = 512;

	if(feedback > minExtend) {
		// fully retract actuator:
		softStartMotor(retract, pwm);

		while(feedback > minExtend) {
			feedback = analogRead(positionFeedbackPin);
		}

		softStopMotor(pwm);
	}
}